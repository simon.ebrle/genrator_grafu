from sys import stdin
import os
from tokenize import Double
import matplotlib.pyplot as plt
import numpy as np
from scipy.optimize import curve_fit as sc

"""
x_value=np.array([30000,27850,25590,23900,22400,20470,19450,18120,17130,16180,15340,14620,13730,13400,12790,12460,12060,11760,11440,11200,10940,10780,10720,10530])
y_value =np.array([23,22,21,20,19,18,17,16,15,14,13,12,11,10,9,8,7,6,5,4,3,2,1,0])
nacitacka =0
"""
file_path = os.path.dirname(os.sys.argv[0]) + "\\imput.txt"
x_value=[]
y_value=[]
nacitacka =0
with open(file_path, 'r',  encoding="utf-8") as p:
    for line in p:
        if nacitacka==23:
            break
        if line.find('//')==-1:
            break 
        
        if line.find("  ")==-1:
            number1 = line.split(',//')
        else :
            line =line.split('  ')
            spec=line[1]
            number1 = spec.split(',//')
        x_value.append(int(number1[0]))

        spec = number1[1] 

        number2 = spec.split('dBm')
        y_value.append(round(float(number2[0])))

        nacitacka = nacitacka +1
"""for line in stdin:
    if nacitacka==23:
        break
    if line.find('//')==-1:
        break 
    print (line)
    number1 =line.split(',//')
    x_value.append(int(number1[0]))
    spec = number1[1] 
    
    number2 = spec.split('dBm')
    y_value.append(round(float(number2[0])))
    nacitacka = nacitacka +1
"""
print (nacitacka)
special1= -944.31*10 #tohle od začátku nedávalo smysl když původní data zašínali od 10530
special2 = 53.35 #tohle to byla původně hodnota 35,71 to mi celí graf posouvalo pořád moc vysoko
special3 =17.64
xt =np.linspace(np.min(x_value)-100,np.max(x_value), np.max(x_value)+100)
print (xt)
popt,pcov=sc(lambda t,a,b,c: a*(np.log10(t+b))-c,  x_value ,y_value)
print (popt[0], popt[1], popt[2])
print("scale :", "{:.2f}".format(popt[0]))
print( "shift: ", "{:.2f}".format(popt[2]))
print("logshift: ", "{:.2f}".format(popt[1]))
plt.plot(xt, popt[0]*(np.log10(xt+(popt[1])))-popt[2], 'r', label='curve 3 params')
#plt.plot(xt,special3*(np.log10(xt+special1))-special2,  'y', label='curve 3 params given')

plt.scatter( x_value, y_value, label= "zisk", color= "blue", marker= "x",s=30)

plt.xlabel('frekvence [Hz]')
plt.ylabel('zisk [dBm]')
# plot title
plt.title('My scatter plot!')
# showing legend
plt.legend()

# function to show the plot

plt.show()



